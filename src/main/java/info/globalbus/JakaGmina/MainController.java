package info.globalbus.JakaGmina;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by globalbus on 31.01.16.
 */
@Controller
public class MainController {
    @RequestMapping("/")
    public String Main(){
        return "index";
    }
}
